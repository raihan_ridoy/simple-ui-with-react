import React from "react";

export default class ReviewRectangle extends React.Component{
    render(){
        let data = this.props.data;
        let image = this.props.image;
        return(
            <div className="panel col-md-3" style={{marginLeft: 20}}>
                <div style={{marginTop: 20}}>
                    <img src={image} height="100" alt="" />
                </div>
                <div className="text-left" style={{marginTop:15}}>
                    <p>{data.description}</p>
                </div>
                <div className="text-center" style={{marginBottom:15}}>
                    <strong>
                    <span style={{fontSize:18}}>{data.name}</span><br />
                    <span style={{fontSize:17}}>{data.designation}</span>
                    </strong>
                </div>
            </div>
        );
    }
}