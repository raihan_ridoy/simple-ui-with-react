import React from "react";

export default class AccountingRectangle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
      let total =0;
      this.props.tableData.map((data,index)=>{
          return  total += data.amount;
      })
    return (
      <div className="panel">
        <div
          className="panel-heading text-left"
          style={{ backgroundColor: "#F0FFF0" }}
        >
          <h4>
            {this.props.title}
          </h4>
        </div>
        <div className="panel-body">
          {this.props.tableData.map((data, index) => {
            return (
              <div className="row" key={index}>
                <div className="col-md-8 text-left">{data.title}</div>
                <div className="col-md-4 text-right">{data.amount}</div>
              </div>
            );
          })}
          <br />
          <div className="row">
            <div className="col-md-8 text-left">
                <label>Total</label>
            </div>
            <div className="col-md-4 text-right">
                <label>{total}</label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
