import React from "react";
import Navbar from "./Navbar";
import wp from "../src/images/wp.PNG";
import openSource from "../src/images/openSource.PNG";
import HeaderRectangle from "./HeaderRectangle";
import featureImage from "../src/images/featureImage.PNG";
import supportImage from "../src/images/supportImage.PNG";

let open = {
  title: "Free & Open Source",
  description:
    "The Core plugin and HR, CRM and Accounting modules are absolutely free to use and also customizable depending on your ideas and needs."
};
let feature = {
  title: "Feature Filled Modules",
  description:
    "Modules are designed to take your business to the next level. If you have a small business, this is ideal for keeping your budget tight but yet get professional results."
};
let support = {
  title: "Support and Docs",
  description:
    "We have a detailed documentation and related videos on any subject related to WP ERP along with round the clock customer support for our valued users."
};

export default class Header extends React.Component {
  render() {
    return (
      <div>
        <div style={{ backgroundColor: "blue" }}>
          <Navbar />
          <div
            className="panel text-left"
            style={{ backgroundColor: "blue", color: "white" }}
          >
            <div className="panel-body text-center">
              <div className="col-md-4  col-md-offset-2">
                <span style={{ fontSize: 40 }}>Supercharge</span>
                <br />
                <span style={{ fontSize: 40 }}>Your Small Business</span>
                <br />
                <span style={{ fontSize: 20 }}>
                  From your WordPress dashboard
                </span>
                <p>
                  WP ERP optimizes your small to medium businesses with powerful
                  HR Manager, CRM & Accounting – Unlock more with 20+ extensions
                  & Project Management module.
                </p>
                <div>                  
                  <button className="btn btn-primary"><i className="fas fa-download"></i> Download Free</button>
                  <button className="btn btn-primary" style={{marginLeft:5}}><i className="fas fa-play"></i> Demo</button>
                  <a href="/" style={{color:"white", marginLeft:5}}>Documentation</a>
                </div>
              </div>
              <div className="col-md-6">
                <img src={wp} alt="" />
              </div>
            </div>
          </div>
        </div>
        <div className="panel">
          <HeaderRectangle image={openSource} data={open} color="white" />
          <HeaderRectangle
            image={featureImage}
            data={feature}
            color="blue"
            textColor="white"
          />
          <HeaderRectangle image={supportImage} data={support} />
        </div>
      </div>
    );
  }
}
