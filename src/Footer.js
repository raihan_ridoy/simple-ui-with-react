import React from "react";

export default class Footer extends React.Component {
  render() {
    return (
      <div className="panel">
        <div
          style={{ backgroundColor: "blue", color: "white" }}
          className="text-center panel-body"
        >
          <div style={{ height: 100 }} />
          <div>
            <h1>Supercharge Your Small Business</h1>
          </div>
          <div>
            <h3>from your WordPress dashboard</h3>
          </div>
          <div>
            <input
              type="button"
              className="btn"
              style={{ color: "blue", marginBottom: 100 }}
              value="Get Started Free"
            />
          </div>
        </div>
        <div style={{ marginTop: 100, marginLeft: 30 }}>
          <div className="col-md-3 text-left">
            <h3>WP ERP</h3>
            <br />
            <div>
              <a href="/">About</a>
            </div>
            <div>
              <a href="/">Refund Policy</a>
            </div>
            <div>
              <a href="/">Terms of Services</a>
            </div>
            <div>
              <a href="/">Support Policy</a>
            </div>
            <div>
              <a href="/">FAQ</a>
            </div>
          </div>
          <div className="col-md-3 text-left">
            <h3>Products</h3>
            <br />
            <div>
              <a href="/">Home</a>
            </div>
            <div>
              <a href="/hrm">HRM</a>
            </div>
            <div>
              <a href="/crm">CRM</a>
            </div>
            <div>
              <a href="/accounting">Accounting</a>
            </div>
            <div>
              <a href="/project">Projects</a>
            </div>
          </div>
          <div className="col-md-3 text-left">
            <h3>Resources</h3>
            <br />
            <div>
              <a href="/">Documentation</a>
            </div>
            <div>
              <a href="/">Discussion Forum</a>
            </div>
            <div>
              <a href="/">Submit Ideas</a>
            </div>
            <div>
              <a href="/">Translet Extensions</a>
            </div>
            <div>
              <a href="/">FAQ</a>
            </div>
          </div>
          <div className="col-md-3 text-left" style={{ marginBottom: 150 }}>
            <h3>Newsletter</h3>
            <br />
            <div>
              <input style={{ backgroundColor: "#E6E6FA" }} type="text" />
            </div>
            <br />
            <div>
              <input
                type="button"
                className="btn btn-primary"
                value="SUBSCRIBE!"
              />
            </div>
          </div>
        </div>
        <div className="panel-footer" style={{ backgroundColor: "bule" }}>
          <div
            className="col-md-6 text-left"
          >
            <i style={{ marginLeft: 10 }} className="fab fa-facebook-f" />
            <i style={{ marginLeft: 10 }} className="fab fa-twitter">
              {" "}
            </i>
            <i style={{ marginLeft: 10 }} className="fab fa-youtube">
              {" "}
            </i>
            <i style={{ marginLeft: 10 }} className="fab fa-google">
              {" "}
            </i>
          </div>
          <div className="col-md-6 text-right">
            <i style={{ marginLeft: 10 }} className="fab fa-google" /> {(new Date().getFullYear())} WP ERP Build by Raihan Ridoy, cell: 01680008381
          </div>
        </div>
      </div>
    );
  }
}
