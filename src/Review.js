import React from "react";
import review1Image from "./images/review1Image.PNG";
import review2Image from "./images/review2Image.PNG";
import otherImage from "./images/otherImage.PNG";
import ReviewRectangle from "./ReviewRectangle";

let review1 = {
    name: "Raihan",
    designation: "CEO",
    description: "I use WPERP and WP Project Manager for myself and my clients. The plugins and add-ons are very well thought out and very impressive, especially for a relatively young product."
}
let review2 = {
    name: "Ridoy",
    designation: "CEO",
    description: "The plugin is unique and serves the purpose very well. One can just install it and start using all features a CRM/HRM needs. Support for this plugin is the best support experience"
}
let review3 = {
    name: "Other",
    designation: "CEO",
    description: "The guys know exactly what they’re doing, and they respond really quickly to resolve quibbles. The guys know exactly what they’re doing, and they respond really quickly to resolve."
}
export default class Review extends React.Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return(
            <div className="well">
                <div className="text-center">
                    <h3>Reviews by our clients</h3>                    
                </div>
                <div className="row col-md-offset-2">
                    <ReviewRectangle image={review1Image} data={review1} />
                    <ReviewRectangle image={review2Image} data={review2} />
                    <ReviewRectangle image={otherImage} data={review3} />
                </div>
                <div className="text-center">
                    <input type="button" className="btn btn-primary" value="Read All The Testimonials" />
                </div>
            </div>
        );
    }
}