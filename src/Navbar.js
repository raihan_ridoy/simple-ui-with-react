import React from "react";

export default class Navbar extends React.Component {
  render() {
    return (
      <nav
        className="navbar"
        style={{ backgroundColor: "blue"}}        
      >
        <div className="container-fluid">
          <div className="navbar-header">
            <a href="/" className="navbar-brand" style={{ color: "white" }}>
              WP ERP
            </a>
          </div>
          <ul className="nav navbar-nav pull-right">
            <li>
              <a
                href="/"
                className="dropdown-toggle"
                data-toggle="dropdown"
                style={{ color: "white" }}
              >
                Modules
                <span className="caret" />
              </a>
              <ul className="dropdown-menu">
                <li>
                  <a href="/">Home</a>
                </li>
              </ul>
            </li>
            <li>
              <a
                href="/"
                className="dropdown-toggle"
                data-toggle="dropdown"
                style={{ color: "white" }}
              >
                Extensions
                <span className="caret" />
              </a>
              <ul className="dropdown-menu">
                <li>
                  <a href="/">Home</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="hrmanagement" style={{ color: "white" }}>
                HRManagement
              </a>
            </li>
            <li>
              <a href="crm" style={{ color: "white" }}>
                CRM
              </a>
            </li>
            <li>
              <a href="accounting" style={{ color: "white" }}>
                Accounts
              </a>
            </li>
            <li>
              <a href="ProjectManager" style={{ color: "white" }}>
                Projects
              </a>
            </li>
            <li>
              <a className="btn-default" style={{ color: "red" }} href="/">
                Signin
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
