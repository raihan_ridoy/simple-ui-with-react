import React from "react";
import CrmRectangle from "./CrmRectangle";
import CRMSideRectangle from "./CRMSideRectangle";
import DayPicker from "react-day-picker";
import ComponenetSideRectangle from "./ComponentSideRectangle";

const birthdayStyle = `.DayPicker-Day--highlighted {
    background-color: orange;
    color: white;
  }`;

const leaveStyle = `.DayPicker-Day--leave {
    background-color: red;
    color: white;
  }`;

const modifiers = {
  highlighted: new Date(),
  leave: new Date(2019, 1, 19)
};

export default class CRM extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      employeeRactangleData: {
        text: "Employee",
        textColor: "#006400"
      },
      todaysSchedule: [],
      upcomingSchedule: [],
      contactList: [
        {
          name: "Raihan",
          phone: "00000000000"
        },
        {
          name: "Ridoy",
          phone: "00000111111"
        }
      ],
      companyList: [
        {
          name: "Latitude",
          phone: "123456789"
        },
        {
          name: "Wedeves",
          phone: "987654321"
        }
      ]
    };
  }

  render() {
    let {
      contactList,
      companyList,
      todaysSchedule,
      upcomingSchedule
    } = this.state;
    return (
      <div className="container well">
        <div className="col-md-3">
          <ComponenetSideRectangle value="crm" />
        </div>
        <div className="col-md-9">
          <div className="panel">
            <div className="panel-heading text-left">
              <h3>CRM Dashbord</h3>
            </div>
            <div className="panel-body">
              <div className="col-md-8">
                <div className="row well">
                  <div className="col-md-5">
                    <CrmRectangle data={"employee"} />
                  </div>
                  <div className="col-md-5 col-md-offset-1">
                    <CrmRectangle data={"company"} />
                  </div>
                </div>
                <div className="row">
                  <div className="panel">
                    <div
                      className="panel-heading text-left"
                      style={{ backgroundColor: "#F0FFF0" }}
                    >
                      <h4>
                        <i className="fas fa-calendar-alt" /> My Schedules
                      </h4>
                    </div>
                    <div
                      className="panel-body"
                      style={{ backgroundColor: "#f5f5f5" }}
                    >
                      <div className="">
                        <style>{birthdayStyle}</style>
                        <style>{leaveStyle}</style>
                        <DayPicker
                          onDayClick={this.onDayChange}
                          modifiers={modifiers}
                          month={new Date()}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="container col-md-3 well"
                style={{ marginLeft: 30 }}
              >
                <CRMSideRectangle data={"today"} schedule={todaysSchedule} />
                <CRMSideRectangle
                  data={"comming"}
                  schedule={upcomingSchedule}
                />
                <div className="panel">
                  <div
                    className="panel-heading text-left"
                    style={{ backgroundColor: "#F0FFF0" }}
                  >
                    <h4>
                      <i className="fas fa-plus" /> Recently Added
                    </h4>
                  </div>
                  <div className="panel-body text-left">
                    <h5>Contact Lists</h5> <br />
                    {contactList.map((contact, index) => {
                      return (
                        <div className="row" key={index}>
                          <div className="col-md-2">
                            <i className="fas fa-user" />
                          </div>
                          <div className="col-md-8">
                            <div className="row">{contact.name}</div>
                            <div className="row">{contact.phone}</div>
                          </div>
                          <div className="col-md-2">
                            <i className="fas fa-times" />
                          </div>
                        </div>
                      );
                    })}
                    <h5>Company List</h5> <br />
                    {companyList.map((company, index) => {
                      return (
                        <div className="row" key={index}>
                          <div className="col-md-2">
                            <i className="fas fa-user" /> F
                          </div>
                          <div className="col-md-8">
                            <div className="row">{company.name}</div>
                            <div className="row">{company.phone}</div>
                          </div>
                          <div className="col-md-2">
                            <i className="fas fa-times" />
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  onDayChange(e) {
    let date = new Date(e);
    console.log(date);
    //will call an api to get specific dates schedule
  }
}
