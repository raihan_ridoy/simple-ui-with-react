import React from "react";

export default class HeaderRectangle extends React.Component{
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        let image = this.props.image;
        let data = this.props.data;
        return(
            <div className="panel col-md-4" style={{backgroundColor: this.props.color,color: this.props.textColor}}>
             <img src={image} alt="" />
             <div className="panel-heading text-center">
                <h3>{data.title}</h3>
             </div>
                <div className="panel-body text-center">
                   <p>{data.description}</p>
                </div>
            </div>
        );
    }
}